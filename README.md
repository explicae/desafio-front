
# README #

### Desafio Front-end ###

O desafio consiste em criar uma Landing Page de Black Friday baseado no layout disponibilizado em PSD. O download do arquivo .psd pode ser feito [aqui](/materiais).

O candidato deve dar fork neste repositório, criar e desenvolver seu projeto de front na pasta projeto e, após a conclusão, realizar um Pull Request para análise do time. 

#### Guia de boas práticas ####
* Prefira ter vários commits descrevendo o que foi feito em cada etapa ao invés de um único commit no final;
* Uma estrutura organizada do projeto o deixa mais eficiente para manutenções;
* Pré-processadores de CSS são excelentes para desenvolver o layout por componentes (dica: nós trabalhamos com SASS);
* Evite o máximo possível o uso do !important no CSS!

#### Após a entrega serão avaliados ####
* Similaridade com o layout proposto
* Responsividade em simulações de dispositivos
* Qualidade do código
* Organização do projeto

#### Linguagens ####

Para desenvolvimento da landing page deve ser utilizado

* HTML5
* CSS3
* jQuery
* Bootstrap 4